# A rofi launcher

A rofi launcher base on nord color

![rofi-nord-launcher](screenshot/rofi-nord-launcher.png)

# License
MIT
